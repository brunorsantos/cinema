<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="pt-br">
<jsp:useBean id="daoFilme"  class="br.com.cinema.Dao.FilmeDao"/>
<jsp:useBean id="filme"  class="br.com.cinema.entity.Filme"/>

<%
String id;
String msg = "Comprar Ingresso";

if(request.getParameter("filme") == null){
	msg = "Filme n�o enviado";
}else{
	id = request.getParameter("filme");
	filme = daoFilme.getById(Integer.parseInt(id));
	System.out.println(filme.getNome());
}
%>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Cinema</title>

   <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="resources/css/freelancer.css" rel="stylesheet">
     

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: 'dd/mm/yyyy',                
                language: 'pt-BR'
            });
        });

    </script>
    <!-- Custom fonts -->
    <link href="resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="resources/http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="resources/http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://localhost:8084/">Cinema Bom</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    
     <!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
            	</br>
                <div class="col-lg-12 text-center">
                    <h2><%=msg %></h2>
                    <hr class="star-primary">
                </div>
            </div>
       
    <div class="row" >
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
	                            <form name="sentMessage" action="CompraServlet" method="post">
			                        <label><%=filme.getNome()%></label>
			                        <img src="<%=filme.getUrlImage()%>" style="width:200px;height: 200px;" class="img-responsive" alt="">
			                        <div class="row control-group">
			                            <div class="form-group col-xs-12 floating-label-form-group controls">
			                                <input type="hidden" value="<%=filme.getId()%>" name="id" />
			                                <input type="text" class="form-control" value="<%=filme.getNome()%>"  name="nome"  />
			                                <p class="help-block text-danger"></p>
			                            </div>
			                        </div>
			                        
			                       		 <br>
			                        		<div> 
										        <input type="text" name="data" class="datepicker" required="required" />
										    </div>	
			                       		 <br></br>
			                       		 <select name="horario" required="required" class="selectpicker">
										  <option>Selecione o Hor�rio</option>
										  <option value="1">15H</option>
										  <option value="2">17H</option>
										  <option value="3">20H</option>
										</select>
										</br></br>
									
				                        <p>Valor UN R$15,00</p>
				                        <input type="hidden" name="valor_unitario01" id="valor_unitario01"  value="15"/> 
			                        	<!-- Valor Unit�rio 01:<input type="text" value="15" disabled name="valor_unitario01" id="valor_unitario01" /> -->
			                        	<div class="row control-group">
				                            <div class="form-group col-xs-12 floating-label-form-group controls">
				                                 <p>QTD</p><input type="text" name="qnt01" id="qnt01" value="0" />
				                                <p class="help-block text-danger"></p>
				                            </div>
			                        	</div> 
										<div class="row control-group">
				                            <div class="form-group col-xs-12 floating-label-form-group controls">
												<h1>TOTAL R$ <input type="text" name="total01" id="total01" readonly="readonly" disabled /></h1>
										  		<p class="help-block text-danger"></p>
				                            </div>
			                        	</div>
			                        	</br></br></br></br>
			                        	
			                        <div id="success"></div>
			                        <div class="row">
			                            <div class="form-group col-xs-12">
			                                <button type="submit" class="btn btn-success btn-lg">Comprar Ingresso</button>
			                                <a href="http://localhost:8084/usuario.jsp">
			                                	<button type="button" class="btn btn-default btn-lg"><i class="fa fa-times"></i>Voltar</button>
			                                </a>
			                            </div>
			                        </div>
	                    	  </form>
	                    	  
	                    	  
	                    	  
	                    	  
	                    	  <script type="text/javascript">
 
								function id( el ){
									        return document.getElementById( el );
									}
									function total01( un01, qnt01 )
									{
									        return un01 * qnt01;
									}
									window.onload = function()
									{
									        id('valor_unitario01').onkeyup = function()
									        {
									                id('total01').value = total01( this.value , id('qnt01').value );
									        }      
									        id('qnt01').onkeyup = function()
									        {
									                id('total01').value = total01( id('valor_unitario01').value , this.value );
									        }
									}
									
									
							</script>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Localiza��o</h3>
                        <p>Rua Jo�ozinho, S/N<br>Centro, RJ</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Redes Sociais</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Sobre o desenvolvedore</h3>
                        <p>Trabalho desenvolvido pelo aluno Bruno Rangel,como parte de avali��o da disciplina. <a href="http://startbootstrap.com">Start Bootstrap</a>.</p>
                    </div>.
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Bruno Rangel
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visible-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>

    
   

   <!-- <!-- jQuery
    <script src="resources/js/jquery.js"></script>

    Bootstrap Core JavaScript--> -->
    <script src="resources/js/bootstrap.min.js"></script> 

    <!-- <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="resources/js/classie.js"></script>
    <script src="resources/js/cbpAnimatedHeader.js"></script>

    <script src="resources/js/jqBootstrapValidation.js"></script>

    <script src="resources/js/freelancer.js"></script> --> -->
    

	<script src="resources/js/bootstrap-datapicker-br.js"></script>
	<script src="resources/js/bootstrap-datapicker.js"></script>


    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</body>

</html>
