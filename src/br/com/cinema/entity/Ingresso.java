package br.com.cinema.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = "Ingresso.findByAll", query = "SELECT i FROM Ingresso i")
})
public class Ingresso {

	public static final String BUSCA_TODOS = "Ingresso.findByAll";
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column
	private String codigoDaCompra;
	
	@Column
	private String horarioDoFilme;
	
	@Column
	private String dataDaSessao;
	
	@Column
	private String nomeDoFilme;
	
	@Column
	private String qtdIngresso;
	
	@Column
	private String precoTotal;
	
	public String getCodigoDaCompra() {
		return codigoDaCompra;
	}


	public void setCodigoDaCompra(String codigoDaCompra) {
		this.codigoDaCompra = codigoDaCompra;
	}


	public String getHorarioDoFilme() {
		return horarioDoFilme;
	}


	public void setHorarioDoFilme(String horarioDoFilme) {
		this.horarioDoFilme = horarioDoFilme;
	}


	public String getDataDaSessao() {
		return dataDaSessao;
	}


	public void setDataDaSessao(String dataDaSessao) {
		this.dataDaSessao = dataDaSessao;
	}


	public String getNomeDoFilme() {
		return nomeDoFilme;
	}


	public void setNomeDoFilme(String nomeDoFilme) {
		this.nomeDoFilme = nomeDoFilme;
	}


	public String getQtdIngresso() {
		return qtdIngresso;
	}


	public void setQtdIngresso(String qtdIngresso) {
		this.qtdIngresso = qtdIngresso;
	}


	public String getPrecoTotal() {
		return precoTotal;
	}


	public void setPrecoTotal(String precoTotal) {
		this.precoTotal = precoTotal;
	}


	public static String getBuscaTodos() {
		return BUSCA_TODOS;
	}

	public int getId() {
		return id;
	}
	

	public void setId(int id) {
		this.id = id;
	}
	
	
}
