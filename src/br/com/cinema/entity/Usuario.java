package br.com.cinema.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = "Usuario.findByAll", query = "SELECT u FROM Usuario u"),
	@NamedQuery(name = "Usuario.findByEmail", query = "SELECT u FROM Usuario u where u.email = :email"),
	@NamedQuery(name = "Usuario.findBySenhaAndEmail", query = "SELECT u FROM Usuario u where u.senha = :senha and u.email = :email")
})
public class Usuario {
	
	public static final String BUSCA_TODOS = "Usuario.findAll";
	public static final String BUSCA_EMAIL = "Usuario.findByEmail";
	public static final String VERIFICA_USER = "Usuario.findBySenhaAndEmail";

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column
	private String email;
	
	@Column
	private String senha;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
