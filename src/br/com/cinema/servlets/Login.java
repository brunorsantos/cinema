package br.com.cinema.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.cinema.Dao.UsuarioDao;
import br.com.cinema.entity.Usuario;
import br.com.cinema.util.EnumUtil;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		
		Usuario user = new Usuario();
        HttpSession sessao = request.getSession();                
        // buscando os par�metros no request
        String email = request.getParameter("email");
        String senha = request.getParameter("senha");
        
        UsuarioDao dao = UsuarioDao.getInstance();
		if(dao.getVerificaUser(senha, email)){
			user.setEmail(email);
			user.setSenha(senha);
			sessao.setAttribute("user", user);
			request.setAttribute("user",user);
			
			
			if(email.equals(EnumUtil.ADMIN.getEmail())){
				// direciona para p�gina de administrador
				System.out.println("� Administrador");
				response.sendRedirect("admin.jsp");
			}else{
				// direciona para uma p�gina s� com os filmes para comprar ingresso
				System.out.println("� usu�rio");
				response.sendRedirect("usuario.jsp");
			}
			
		}else{
			request.setAttribute("msg", "Voc� precisa fazer um cadastro ou verificar seu login e senha");
			RequestDispatcher despachar = request.getRequestDispatcher("index.jsp");
			despachar.forward(request, response);
			System.out.println("Usu�rio ou senha incorretos");
		}
	}

}
