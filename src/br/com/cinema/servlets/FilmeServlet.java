package br.com.cinema.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.cinema.Dao.FilmeDao;
import br.com.cinema.entity.Filme;

/**
 * Servlet implementation class FilmeServlet
 */
@WebServlet("/FilmeServlet")
public class FilmeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FilmeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		Filme filme = new Filme();
		
		filme.setNome(request.getParameter("nome"));
		filme.setUrlImage(request.getParameter("URLImage"));
		filme.setUrlVideo(request.getParameter("URLVideo"));
		
		
		
		FilmeDao dao = new FilmeDao();
		
		try {
			dao.salvar(filme);
			request.setAttribute("msg", "Filme Cadastrado Com Sucesso!!!");
			RequestDispatcher despachar = request.getRequestDispatcher("admin.jsp");
			despachar.forward(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
