package br.com.cinema.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.cinema.Dao.UsuarioDao;
import br.com.cinema.entity.Usuario;

@WebServlet("/CadastroServlet")
public class CadastroServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CadastroServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void init(ServletConfig config) throws ServletException {
        super.init(config);
        log("Iniciando a servlet");
    }

	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);

		Usuario user = new Usuario();
		// busca o writer
		PrintWriter out = response.getWriter();

		// buscando os par�metros no request
		String email = request.getParameter("email");
		String senha = request.getParameter("senha");
		if (!email.equals("") || email != null && !senha.equals("") || senha != null) {
			user.setEmail(email);
			user.setSenha(senha);
		}

		UsuarioDao dao = UsuarioDao.getInstance();
		
		if(dao.getByEmail(email)) {
			request.setAttribute("msg", "Email j� est� cadastrado!!!");
			RequestDispatcher despachar = request.getRequestDispatcher("index.jsp");
			despachar.forward(request, response);
		}else{
			try {
				dao.salvar(user);
				try {
					request.setAttribute("msg", "Cadastrado com Sucesso!!!");
					RequestDispatcher despachar = request.getRequestDispatcher("index.jsp");
					despachar.forward(request, response);
					// response.sendRedirect("index.jsp");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

	}
	
	public void destroy() {
        super.destroy();
        log("Destruindo a servlet");
    }
}
