package br.com.cinema.servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.cinema.Dao.IngressoDao;
import br.com.cinema.entity.Ingresso;
import br.com.cinema.entity.Usuario;

/**
 * Servlet implementation class CompraServlet
 */
@WebServlet("/CompraServlet")
public class CompraServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CompraServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
		
		Usuario user = new Usuario();
		String id = request.getParameter("id");
		String nomeDoFilme = request.getParameter("nome");
		String dataDaSessao = request.getParameter("data");
		String horarioDoFilme = formataHorario(request.getParameter("horario"));
		String qtdIngresso = request.getParameter("qnt01");
		//String precoTotal = request.getParameter("total01");// n�o pega por causa do disabled
		
		// Converter a quantidade em INT para multiplicar por 15
		Integer precoTotal = 15 * Integer.parseInt(qtdIngresso);
		
		String codigoDaCompra = getCodigo();
		
		Ingresso f  = new Ingresso();
		
		f.setCodigoDaCompra(codigoDaCompra);
		f.setDataDaSessao(dataDaSessao);
		f.setHorarioDoFilme(horarioDoFilme);
		f.setNomeDoFilme(nomeDoFilme);
		f.setPrecoTotal(precoTotal.toString());
		f.setQtdIngresso(qtdIngresso);
		
		 
		
		//f.setCodigo(codigo);
		
		
		IngressoDao dao = new IngressoDao();
		//Usuario user = (Usuario)((HttpServletRequest) request).getSession().getAttribute("user");
		try {
			request.setAttribute("user",user);
			 dao.salvar(f);
			 
			 	request.setAttribute("msg", "Compra feita com Sucesso!!!");
			 	request.setAttribute("nomeDoFilme", nomeDoFilme);
			 	request.setAttribute("dataDaSessao", dataDaSessao);
			 	request.setAttribute("horarioDoFilme", horarioDoFilme);
			 	request.setAttribute("qtdIngresso", qtdIngresso);
			 	request.setAttribute("precoTotal", precoTotal.toString());
			 	request.setAttribute("codigoDaCompra", codigoDaCompra);
				RequestDispatcher despachar = request.getRequestDispatcher("comprovante.jsp");
				despachar.forward(request, response);
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String formataHorario(String horario){
		
		if(horario.equals("1")){
			return  "15H";
		}else if(horario.equals("2")){
			return  "17H";
		}else{
			return "20H";
		}
	}
	
	public String getCodigo(){
		String codigo = "" ;
		Random gera = new Random();
		for (int i = 0; i < 12; i++) {
			codigo += gera.nextInt(9);
		}
		
		return codigo;
	}

}
