package br.com.cinema.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.cinema.Dao.FilmeDao;
import br.com.cinema.entity.Filme;
import br.com.cinema.entity.Usuario;

/**
 * Servlet implementation class Remove
 */
@WebServlet("/Remove")
public class Remove extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Remove() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		
Filme f  = new Filme();
		
		f.setId(Integer.parseInt(request.getParameter("id")));
		f.setNome(request.getParameter("nome"));
		f.setUrlImage(request.getParameter("URLImage"));
		f.setUrlVideo(request.getParameter("URLVideo"));
		FilmeDao dao = new FilmeDao();
		Usuario user = (Usuario)((HttpServletRequest) request).getSession().getAttribute("user");
		try {
			request.setAttribute("user",user);
			 dao.remove(f);
			 
			 	request.setAttribute("msg", "Exclus�o Realizada Com Sucesso!!!");
				RequestDispatcher despachar = request.getRequestDispatcher("admin.jsp");
				despachar.forward(request, response);
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
