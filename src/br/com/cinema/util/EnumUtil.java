package br.com.cinema.util;

public enum EnumUtil {

	ADMIN("admin@admin.com");
	
	
	
	private String email;
	
	EnumUtil(String email){
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
