package br.com.cinema.util;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.com.cinema.entity.Usuario;

/**
 * Servlet Filter implementation class FilterLogin
 */
//@WebFilter("/*")
@WebFilter(displayName = "FilterLogin", urlPatterns = {"/admin.jsp", "/edit.jsp", "/remove.jsp", "/cadastro.jsp", "/lista-ingressos.jsp"}) 
public class FilterLogin implements Filter {

    /**
     * Default constructor. 
     */
    public FilterLogin() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		// pass the request along the filter chain
		Usuario user = (Usuario)((HttpServletRequest) request).getSession().getAttribute("user");
		
		if(user == null){
			
			RequestDispatcher despachar = request.getRequestDispatcher("index.jsp");
			despachar.forward(request, response);
		}
		else if(!user.getEmail().equals(EnumUtil.ADMIN.getEmail())){
			String contextPath = ((HttpServletRequest) request).getContextPath();
			request.setAttribute("user", user);
			((HttpServletResponse) response).sendRedirect
            (contextPath + "/usuario.jsp");
		}else{
			
			chain.doFilter(request, response);
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
