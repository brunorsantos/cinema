package br.com.cinema.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import br.com.cinema.entity.Ingresso;

public class IngressoDao {
	private static IngressoDao instance;
	protected EntityManager entityManager;

	public IngressoDao() {
		entityManager = getEntityManager();
	}

	public static IngressoDao getInstance() {
		if (instance == null) {
			instance = new IngressoDao();
		}
		return instance;
	}

	private EntityManager getEntityManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory( "cinema" );
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}

		return entityManager;
	}

	public Ingresso getById(final int id) throws SQLException {
		return entityManager.find(Ingresso.class, id);
	}

	public void salvar(Ingresso f) throws SQLException {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(f);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void update(Ingresso f) throws SQLException {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(f);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void remove(Ingresso f) throws SQLException {
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(entityManager.getReference(Ingresso.class, f.getId()));
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public List<Ingresso> getLista() throws SQLException {
		TypedQuery<Ingresso> query = entityManager.createNamedQuery(Ingresso.BUSCA_TODOS, Ingresso.class);
		if (!query.getResultList().isEmpty()) {
			List<Ingresso> Ingressos = new ArrayList<>();
			Ingressos = query.getResultList();
			return Ingressos;
		}
		return null;
	}

}
