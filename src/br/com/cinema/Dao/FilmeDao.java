package br.com.cinema.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import br.com.cinema.entity.Filme;

public class FilmeDao {

	private static FilmeDao instance;
	protected EntityManager entityManager;

	public FilmeDao() {
		entityManager = getEntityManager();
	}

	public static FilmeDao getInstance() {
		if (instance == null) {
			instance = new FilmeDao();
		}
		return instance;
	}

	private EntityManager getEntityManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory( "cinema" );
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}

		return entityManager;
	}

	public Filme getById(final int id) throws SQLException {
		return entityManager.find(Filme.class, id);
	}

	public void salvar(Filme f) throws SQLException {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(f);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void update(Filme f) throws SQLException {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(f);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void remove(Filme f) throws SQLException {
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(entityManager.getReference(Filme.class, f.getId()));
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public List<Filme> getLista() throws SQLException {
		TypedQuery<Filme> query = entityManager.createNamedQuery(Filme.BUSCA_TODOS, Filme.class);
		if (!query.getResultList().isEmpty()) {
			List<Filme> filmes = new ArrayList<>();
			filmes = query.getResultList();
			return filmes;
		}
		return null;
	}

}
