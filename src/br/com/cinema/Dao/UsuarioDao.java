package br.com.cinema.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import br.com.cinema.entity.Usuario;

public class UsuarioDao {
	private static UsuarioDao instance;
	protected EntityManager entityManager;

	private UsuarioDao() {
		entityManager = getEntityManager();
	}

	public static UsuarioDao getInstance() {
		if (instance == null) {
			instance = new UsuarioDao();
		}
		return instance;
	}

	private EntityManager getEntityManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("cinema");
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}

		return entityManager;
	}

	public Usuario getById(final Long long1) throws SQLException {
		return entityManager.find(Usuario.class, long1);
	}
	
	public Boolean getByEmail(String email){
		TypedQuery<Usuario> query = entityManager.createNamedQuery(Usuario.BUSCA_EMAIL, Usuario.class);
		query.setParameter("email", email);
		if (query.getResultList().isEmpty()) {

			return false;
		} else {
			return true;
		}
	}
	
	public Boolean getVerificaUser(String senha, String email){
		TypedQuery<Usuario> query = entityManager.createNamedQuery(Usuario.VERIFICA_USER, Usuario.class);
		query.setParameter("senha", senha);
		query.setParameter("email", email);
		if (query.getResultList().isEmpty()) {

			return false;
		} else {
			return true;
		}
	}

	public void salvar(Usuario u) throws SQLException {

		try {
			entityManager.getTransaction().begin();
			entityManager.persist(u);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void update(Usuario u) throws SQLException {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(u);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void remove(Usuario u) throws SQLException {
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(u);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public List<Usuario> findAll() throws SQLException {
		TypedQuery<Usuario> query = entityManager.createNamedQuery(Usuario.BUSCA_TODOS, Usuario.class);
		if (!query.getResultList().isEmpty()) {
			List<Usuario> usuarios = new ArrayList<>();
			usuarios = query.getResultList();
			return usuarios;
		}
		return null;
	}

}
